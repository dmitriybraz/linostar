"use strict";
$(document).ready(function() {
	$(".hdr_slider").slick({
		speed: 500,
		fade: true,
		nextArrow:
			'<button type="button" class="slick-next"><img src="./img/b1_sldr_next.png" alt=""></button>',
		prevArrow:
			'<button type="button" class="slick-prev"><img src="./img/b1_sldr_prev.png" alt=""></button>',
		responsive: [
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					dots: true
				}
			}
		]
	});

	$(".doc_slider").slick({
		speed: 500,
		infinite: true,
		slidesToShow: 3,
		adaptiveHeight: true,
		nextArrow:
			'<button type="button" class="slick-next"><img src="./img/b1_sldr_next.png" alt=""></button>',
		prevArrow:
			'<button type="button" class="slick-prev"><img src="./img/b1_sldr_prev.png" alt=""></button>',
		responsive: [
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					dots: true,
					slidesToShow: 1
				}
			}
		]
	});

	$(".b1_btn_dn").on("click", function(e) {
		e.preventDefault();
		$("html, body").animate(
			{
				scrollTop: $(".block2").offset().top
			},
			"300"
		);
	});

	$(function() {
		$(".tel").mask("+7 (999) 999-99-99");
	});

	$(".menu_btn").on("click", function(e) {
		e.preventDefault();
		$(".m_menu").toggleClass("menu_active");
		$(".menu_btn").toggleClass("menu_btn_active");
		$(".m_menu_back").toggleClass("m_menu_back_active");
		$(".menu_btn i").toggleClass("m_menu_btn_show");
	});

	$(".m_menu_back").on("click", function() {
		$(".m_menu").toggleClass("menu_active");
		$(".menu_btn").toggleClass("menu_btn_active");
		$(".m_menu_back").toggleClass("m_menu_back_active");
		$(".menu_btn i").toggleClass("m_menu_btn_show");
	});

	$("#modal_buy").on("show.bs.modal", function(event) {
		var button = $(event.relatedTarget);
		var recipient = button.data("title");
		var modal = $(this);
		modal.find(".item_title").text(recipient);
	});

	$(".collapse").on("show.bs.collapse", function(e) {
		let a = $(e.target);
		let b = $(a).parent();
		b.addClass("doc_collapse_frame_active");
	});
	$(".collapse").on("hide.bs.collapse", function(e) {
		let a = $(e.target);
		let b = $(a).parent();
		b.removeClass("doc_collapse_frame_active");
	});
});
